import java.util.ArrayList;


public class Machines {

	
	
	ArrayList<String> lip;
	ArrayList<Packet> nD;
	ArrayList<Packet> nA;
	private String nomMachine;
	private Network net;
	
	public Machines(String nom,Network net){
		this.lip = new ArrayList<String>();
		this.nD = new ArrayList<Packet>();
		this.nA = new ArrayList<Packet>();
		this.nomMachine=nom;
		this.net = net;
		this.net.getListMachine().add(this);
		
	}
	
	public ArrayList<Packet> getnD() {
		return nD;
	}

	public void setnD(Packet nD) {
		this.nD.add(nD);
	}

	public ArrayList<Packet> getnA() {
		return nA;
	}

	public void setnA(Packet nA) {
		this.nA.add(nA);
	}

	public void setLip(ArrayList<String> lip) {
		this.lip = lip;
	}

	public void envoiMessage(Message message){
		
	}
	/*
	public String ecouterMessage(){
		String res=null;
		for (int i = 0; i < net.lmess.size(); i++) {
			for (int j = 0; j < lip.size(); j++) {
				if(net.lmess.get(i).getDestination() == this.lip.get(j)){
					res = net.lmess.get(i).getContenu();
				}
			}
			
		}
		return res;
		
	}

	*/

	public ArrayList<String> getLip() {
		return lip;
	}

	public String getIP(int i){
		return this.lip.get(i);
	}

	public Network getNet() {
		return net;
	}

	public void setNet(Network net) {
		this.net = net;
	}

	public void setIp(String ip) {
		this.lip.add(ip);
	}

	public String getNomMachine() {
		return nomMachine;
	}

	public void setNomMachine(String nomMachine) {
		this.nomMachine = nomMachine;
	}
}
