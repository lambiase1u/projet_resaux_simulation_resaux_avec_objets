import java.util.ArrayList;



public class Network {
	
	String nom;
	int nbmess;
	int nbnetwork;

	ArrayList<Packet> np;
	ArrayList<Machines> lmachines;
	
	public Network(String nom){
		 this.nom = nom;
		 this.np = new ArrayList<Packet>();

		 this.lmachines = new ArrayList<Machines>();
		 this.nbmess=0;
		 this.nbnetwork++;
	}
	
	

	public ArrayList<Packet> getNp() {
		return np;
	}



	public void setNp(ArrayList<Packet> np) {
		this.np = np;
	}



	public int getNbnetwork() {
		return nbnetwork;
	}



	public void setNbnetwork(int nbnetwork) {
		this.nbnetwork = nbnetwork;
	}


	public ArrayList<Machines> getListMachine(){
		return lmachines;
	}
	public int getNbmess() {
		return nbmess;
	}

	public void setNbmess(int nbmess) {
		this.nbmess = nbmess;
	}

	
	public String getNom(){
		return this.nom;
	}
	
	
}
