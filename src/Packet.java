
public class Packet {
	
	private String contenu;
	private String destination;
	
	public Packet(String destination, String contenu){
		this.destination=destination;
		this.contenu=contenu;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
