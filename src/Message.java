
public class Message extends Packet{
	
	private String contenu;
	private String destination;
	
	public Message(String destination, String contenu){
		super(destination,contenu);
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
